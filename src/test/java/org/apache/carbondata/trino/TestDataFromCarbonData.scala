/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.carbondata.trino

import org.apache.spark.SparkContext
import org.apache.spark.sql.CarbonSession._
import org.apache.spark.sql.SparkSession

import scala.util.Random

/**
 * @Description Build Test Data Store AS CarbonData
 * @Author chenzhengyu
 * @Date 2021-09-10 20:37
 */
object TestDataFromCarbonData {

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext();
    val spark = SparkSession.builder().enableHiveSupport().getOrCreate()
    val carbon = SparkSession.builder().config(sc.getConf).getOrCreateCarbonSession("hdfs://localhost:9000/user/hive/warehouse")
    //build random num
    val randomGameId = new Random(5);
    val randomMoney = new Random();
    import spark.implicits._
    //build data
    val dfTemp = spark.sparkContext.parallelize(1 to 1000000000).map(x => (randomGameId.nextInt(5), x % 8, "20210920" + (x % 100), x % 5, System.currentTimeMillis() + x % 500L, System.currentTimeMillis() + x % 500L, randomGameId.nextInt(4), randomMoney.nextInt(1000))).toDF("gameId", "unionId", "orderId", "userId", "createTime", "payTime", "status", "money")
    //create temp view
    dfTemp.createOrReplaceTempView("temp")
    //create table
    carbon.sql("CREATE TABLE CARBONDATA_PAYMENT (`gameId` int, `unionId` int, `orderId` STRING, `userId` INT, `createTime` bigint, `payTime` bigint,`status` int,`money` int ) stored as carbondata")
    //show tables
    carbon.sql("show tables").show
    //insert data and select
    spark.sql("insert into table CARBONDATA_PAYMENT select * from temp")
    spark.sql("select count(1) from CARBONDATA_PAYMENT").show()
    sc.stop()
  }
}
